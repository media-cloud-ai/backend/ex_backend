FROM elixir:1.13.3-alpine AS ex_builder

RUN apk update && \
    apk add --no-cache \
    ca-certificates \
    curl \
    gawk \
    git \
    make \
    nodejs \
    npm \
    python3 \
    tar \
    wget \
    g++ \
    && \
    mix local.hex --force && \
    mix local.rebar --force && \
    mix hex.info

WORKDIR /app
ENV MIX_ENV prod
ENV PATH /root/.yarn/bin:/root/.config/yarn/global/node_modules/.bin:$PATH
ADD . .

RUN mix deps.get && \
    mix compile && \
    mix distillery.release --env=$MIX_ENV && \
    mix generate_documentation && \
    cd assets && \
    npm install && \
    npm install node-gyp && \
    npm install bcrypt && \
    npm run lint && \
    npm run release && \
    cd .. && \
    mix openapi.stepflow && \
    mix openapi.backend && \
    mix phx.digest

FROM alpine:3.15

WORKDIR /app

ARG customAppPort=8080

ENV PORT $customAppPort

RUN apk update && \
    apk add bash openssl curl libstdc++

COPY --from=ex_builder /app/_build/prod/rel/ex_backend .
COPY --from=ex_builder /app/priv/static static/
COPY --from=ex_builder /app/documentation.json .

RUN apk add --no-cache tzdata

RUN backend="$(ls -1 lib/ | grep ex_backend)" && \
    rm -rf lib/$backend/priv/static/ && \
    mv static/ lib/$backend/priv/

HEALTHCHECK --interval=30s --start-period=2s --retries=2 --timeout=3s CMD curl -v --silent --fail http://localhost:$PORT/ || exit 1

CMD ["./bin/ex_backend", "foreground"]

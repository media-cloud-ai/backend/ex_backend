import { Component } from '@angular/core'

@Component({
  selector: 'secrets-component',
  templateUrl: 'secrets.component.html',
  styleUrls: ['./secrets.component.less'],
})
export class SecretsComponent {}

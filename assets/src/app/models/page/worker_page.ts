import { Worker } from '../worker'

export class WorkerPage {
  data: Worker[]
  total: number
}

export class WorkerData {
  data: Worker
}

import { NotificationEndpoint } from '../notification_endpoint'

export class NotificationEndpointPage {
  data: NotificationEndpoint[]
  total: number
}

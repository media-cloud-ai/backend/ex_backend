import { NotificationTemplate } from '../notification_template'

export class NotificationTemplatePage {
  data: NotificationTemplate[]
  total: number
}

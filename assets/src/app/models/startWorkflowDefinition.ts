export class StartWorkflowDefinition {
  workflow_identifier?: string
  parameters?: Record<string, unknown>
  reference?: string
  version_major?: number
  version_minor?: number
  version_micro?: number
}
